function calculoImc (peso, altura){
  let imc = peso / (altura * altura);
  let resposta = '';
  if(imc < 18.5){
    resposta ='Abaixo do peso';
  } else if(imc >= 18.5 && imc <= 25){
   resposta ='Peso normal';
  } else if(imc > 25 && imc <= 30){
    resposta ='Acima do normal';
  } else if(imc > 30){
    resposta ='Obeso';
  }
  return resposta;
}

console.log(calculoImc(80, 1.70));