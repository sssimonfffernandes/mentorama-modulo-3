function paisTemperatura (pais, temperatura){
  let resultado = '';
  if (pais === 'Brasil' || pais === 'India'){
    temperatura = (temperatura  + 32) * 1.8;
    resultado = temperatura;  
  } else if (pais === 'USA'){
    temperatura = (temperatura - 32 ) / 1.8;
     resultado = temperatura;
  } else {
    console.log('País inválido.')
  }
  return resultado;
}
console.log(paisTemperatura('India' , 33));
// console.log(paisTemperatura('Brasil' , 26));
// console.log(paisTemperatura('USA' , 72));