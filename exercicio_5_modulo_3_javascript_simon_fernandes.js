function proximoDia (dia, mes, ano){
  if (mes === 1 || mes === 4 || mes === 6 || mes === 9 || mes === 11){
    if (dia > 0 && dia < 30){
      dia += 1;
    } else if (dia > 30){
      mes += 1;
      dia = 1;
    }   
} else if (mes === 2){
    if (dia > 0 && dia < 28){
      dia += 1;
    } else if (dia >= 28){
      dia = 1;
      mes = 3;
    }
} else if (mes === 3 || mes === 5 || mes === 7 || mes === 8 || mes === 10 || mes === 12){
  if (dia > 0  && dia < 31){
    dia += 1;
  } else if (dia >= 31){
    mes += 1;
    dia = 1;
  }  
} else {
  dia = 'dia inválido';
} 
return `${dia}/${mes}/${ano}`
}
console.log(proximoDia(3, 11, 2020));